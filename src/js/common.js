var $ = require("jquery");
var common = common || {};

common.get_filename = function() {
    let split = window.location.href.split('/');
    let fileName = split[split.length - 1];
    fileName = common.prettify_text(fileName);
    return fileName
}

common.get_headline = function() {
    let headline = common.get_filename().replace("_", " ");
    return headline;
}

common.get_title = function() {
    let split = common.get_filename().split('/');

    let title = split[split.length - 1].replace("_", " ");
    return title;
}

common.set_title = function() {
    document.getElementsByTagName('title')[0].innerHTML = common.get_title();
}

common.prettify_text = function(text) {
    return text.replace(/_/g, ' ').replace('.html?o=0', '').replace('.html', '');
}

common.get_sub_path = function(text) {
    if (text === 'Informatik') {
        return __DOMAIN_ROOT__ + '/';
    }
    const path = window.location.href;
    return path.slice(0, path.indexOf(text) + text.length);
}

common.populate_fields = function() {
    let headline = $("#headline");
    if (headline[0] && headline[0].innerText == ""){
        let text = common.get_headline();
        headline.text(text);
    }
    let breadcrumb = $("#ovk-breadcrumb");
    let title = $("title");
    let path = window.location.href;

    const start = '/Informatik/';
    const index = path.indexOf(start);

    if (index !== -1) {
        let breadcrumbArray = path.substr(index + start.length).split('/');
        let list = document.createElement("ul");
        list.className = "ovk-breadcrumb";

        breadcrumb.append(list);

        breadcrumbArray.forEach((element) => {
            let elem = document.createElement("li");
            let link = document.createElement("a");
            link.href = common.get_sub_path(element);
            link.innerText = common.prettify_text(element);
            elem.append(common.prettify_text(element));
            list.append(elem);
        });
    }
}

// function being called on each page
common.init = function() {
    $( document ).ready(function() {
        common.populate_fields();
        // Test if page is shown in opal or directly
        if (document.getElementsByClassName('course').length == 0){
            document.getElementById('hamburger-container').style.display = "block";
            document.getElementById('home-icon').style.display = "block";
        }        
    });
}

common.impressum = function() {
    const elem = $('#impressum');

    if (elem.length == 1) {
        fetch('https://tu-dresden.de/impressum').then(function (response) {
        // The API call was successful!
        return response.text();
    }).then(function (html) {
        // This is the HTML from our response as a text string
        common.includeImpressum(html);
    })
    .catch(function (err) {
        // There was an error
        console.warn('Something went wrong.', err);
    });
    }
}

common.includeImpressum = function(data){
    document.getElementById("impressum").innerHTML = data;
}

$('#hamburger-menu').click(() => {
    $(".dropdown-content").toggleClass("show");
});


$(document).click(function(e) {           
    if(!$(e.target).hasClass('dropdown-icon') && !$(e.target).parent().hasClass('dropdown-icon')){
        if ($("#dropdown-content").hasClass('show')) {
            $("#dropdown-content").removeClass('show');
        }     
    }
}); 

common.init();
//common.impressum();